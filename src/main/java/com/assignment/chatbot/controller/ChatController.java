package com.assignment.chatbot.controller;

import com.assignment.chatbot.dto.ReplyDTO;
import com.assignment.chatbot.dto.ResponseDTO;
import com.assignment.chatbot.service.ChatService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("chat")
public class ChatController {

    @Autowired
    ChatService chatService;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseDTO getAllChat(){
        log.info("---------------------------Received get chat request------------------");
        return chatService.getAllChat();
    }

    @RequestMapping(value = "/getChatMessages", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseDTO getActiveChat(@RequestParam Long chatId){
        log.info("---------------------------Received get chat request------------------");
        return chatService.getChatMessages(chatId);
    }

    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseDTO saveUserReply(@RequestParam Long chatId, @RequestBody ReplyDTO response){
        log.info("---------------------------Received reply from user -----------------");
        return chatService.saveUserReply(chatId, response.getReply());
    }
}
