package com.assignment.chatbot.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class ChatDto implements Serializable {

    private Long id;

    private Long customerId;
    private String customerFirstName;
    private String customerLastName;

    private Long employeeId;
    private String employeeFirstName;
    private String employeeLastName;
}
