package com.assignment.chatbot.dto;

import com.assignment.chatbot.enums.ChatType;
import com.assignment.chatbot.enums.Status;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.sql.Timestamp;

@Getter
@Setter
public class ChatMessageDTO implements Serializable {

    private Long id;
    private Long chatId;
    private Timestamp createdAt;
    private Status status;
    private ChatType chatType;
    private String message;
}
