package com.assignment.chatbot.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class EmployeeDTO implements Serializable {
    private Long id;
    private String firstName;
    private String lastName;
}
