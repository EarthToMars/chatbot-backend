package com.assignment.chatbot.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.io.Serializable;

@Getter
@Setter
public class ResponseDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    Object content;
    boolean isSuccessful = true;
    String message;
    HttpStatus httpStatus;

    public ResponseDTO(Object content, boolean isSuccessful, String message, HttpStatus httpStatus) {
        this.content = content;
        this.isSuccessful = isSuccessful;
        this.message = message;
        this.httpStatus = httpStatus;
    }
}
