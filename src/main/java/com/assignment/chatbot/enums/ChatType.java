package com.assignment.chatbot.enums;

public enum ChatType {
    SEND(1),
    RECEIVED(2);

    public final int value;

    ChatType(int value){
        this.value = value;
    }
}
