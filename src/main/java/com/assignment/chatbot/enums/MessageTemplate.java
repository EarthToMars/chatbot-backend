package com.assignment.chatbot.enums;

public enum MessageTemplate {

    TEMPLATE_1(1,"Hi $customerFirstName,\nHope your phone no# is still the same.\nTalk to you soon.\n$employeeFirstName"),
    TEMPLATE_2(2,"Hi $customerFirstName,\nThis is the first reminder.\nTrying to reach you.\n$employeeFirstName"),
    TEMPLATE_3(3,"Hi $customerFirstName,\nThis is the second reminder.\nTrying to reach you.\n$employeeFirstName"),
    TEMPLATE_4(4,"Hi $customerFirstName,\nThis is the last reminder.\nTrying to reach you.\n$employeeFirstName"),
    ;

    public final int value;
    public final String message;

    MessageTemplate(int value, String message) {
        this.value = value;
        this.message = message;
    }

    public static MessageTemplate getTemplate(int value){
        switch (value){
            case 1:
                return TEMPLATE_1;
            case 2:
                return TEMPLATE_2;
            case 3:
                return TEMPLATE_3;
            case 4:
                return TEMPLATE_4;
            default:
                return TEMPLATE_1;
        }
    }
}
