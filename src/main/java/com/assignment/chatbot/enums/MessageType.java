package com.assignment.chatbot.enums;

public enum MessageType {

    EMAIL("email"),
    SMS("sms");

    public final String value;

    MessageType(String value){
        this.value = value;
    }
}
