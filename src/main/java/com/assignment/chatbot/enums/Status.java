package com.assignment.chatbot.enums;

public enum Status {
    ACTIVE(1),
    CLOSED(2);

    public final int value;

    Status(int value){
        this.value = value;
    }
}
