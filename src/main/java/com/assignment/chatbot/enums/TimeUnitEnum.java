package com.assignment.chatbot.enums;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

public enum TimeUnitEnum {

    SECONDS(Calendar.SECOND),
    MINUTES(Calendar.MINUTE),
    HOURS(Calendar.HOUR_OF_DAY),
    DAYS(Calendar.DATE);

    public final int timeUnit;

    TimeUnitEnum(int timeUnit){
        this.timeUnit = timeUnit;
    }
}
