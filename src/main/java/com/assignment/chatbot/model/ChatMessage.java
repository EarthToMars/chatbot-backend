package com.assignment.chatbot.model;

import com.assignment.chatbot.enums.ChatType;
import com.assignment.chatbot.enums.Status;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Getter
@Setter
public class ChatMessage {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Timestamp createdAt;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "chat")
    private Chat chat;

    private Status status;

    private ChatType type;

    private String message;
}
