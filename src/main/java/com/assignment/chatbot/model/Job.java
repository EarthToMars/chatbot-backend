package com.assignment.chatbot.model;


import com.assignment.chatbot.enums.MessageTemplate;
import com.assignment.chatbot.enums.MessageType;
import com.assignment.chatbot.enums.Status;
import com.assignment.chatbot.enums.TimeUnitEnum;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Job {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Status status;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customer")
    private Customer customer;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employee")
    private Employee employee;

    private MessageType messageType;

    private int delayValue;

    private TimeUnitEnum timeUnit;

    private MessageTemplate template;
}
