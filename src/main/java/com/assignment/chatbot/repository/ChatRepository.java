package com.assignment.chatbot.repository;

import com.assignment.chatbot.enums.Status;
import com.assignment.chatbot.model.Chat;
import com.assignment.chatbot.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChatRepository extends JpaRepository<Chat, Long> {
    Chat findByEmployeeIdAndCustomerIdAndStatus(Long employeeId, Long customerId, Status status);

    List<Chat> findByStatus(Status status);
}
