package com.assignment.chatbot.repository;

import com.assignment.chatbot.enums.MessageTemplate;
import com.assignment.chatbot.enums.MessageType;
import com.assignment.chatbot.enums.Status;
import com.assignment.chatbot.enums.TimeUnitEnum;
import com.assignment.chatbot.model.Job;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface JobRepository extends JpaRepository<Job, Long> {

    List<Job> findByStatus(Status status);

    Job findByStatusAndCustomerIdAndEmployeeIdAndTemplate
            (Status status, Long customerId, Long employeeId, MessageTemplate template);

    List<Job> findByStatusAndCustomerIdAndEmployeeId
            (Status status, Long customerId, Long employeeId);
}
