package com.assignment.chatbot.service;

import com.assignment.chatbot.dto.ChatDto;
import com.assignment.chatbot.dto.ChatMessageDTO;
import com.assignment.chatbot.dto.ResponseDTO;
import com.assignment.chatbot.enums.ChatType;
import com.assignment.chatbot.enums.Status;
import com.assignment.chatbot.enums.TimeUnitEnum;
import com.assignment.chatbot.model.*;
import com.assignment.chatbot.repository.*;
import com.assignment.chatbot.util.AsyncSendMessageTask;
import com.assignment.chatbot.util.ScheduledTasksHandler;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.ContextStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import javax.annotation.PostConstruct;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class ChatService {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private ChatRepository chatRepository;

    @Autowired
    private ChatMessageRepository chatMessageRepository;

    @Autowired
    private JobRepository jobRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private ScheduledTasksHandler tasksHandler;

    @Autowired
    private ObjectFactory<AsyncSendMessageTask> taskFactory;

    @Autowired
    @Qualifier("transactionManager")
    protected PlatformTransactionManager txManager;

    public ResponseDTO getAllChat(){
        try{
            List<Chat> chatList = chatRepository.findAll();

            List<ChatDto> chatDtos = new ArrayList<>();
            if(!chatList.isEmpty()){
                for(Chat chat : chatList){
                    ChatDto chatDto = modelMapper.map(chat, ChatDto.class);
                    chatDtos.add(chatDto);
                }
                return new ResponseDTO(chatDtos, true, "Success", HttpStatus.OK);
            }else{
                return new ResponseDTO(null, false, "No chat found", HttpStatus.NOT_FOUND);
            }
        }catch (Exception e){
            e.printStackTrace();
            log.error("Error occurred:"+e.getMessage());
            return new ResponseDTO(null, false, "Some error occurred", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseDTO getChatMessages(Long chatId){
        try {
            log.info("Checking active chat");
            Chat chat = chatRepository.findById(chatId).orElse(null);

            if (chat != null) {
                List<ChatMessage> chatMessages = chatMessageRepository.findByChatIdOrderByCreatedAtAsc(chat.getId());
                List<ChatMessageDTO> chatMessageDTOS = new ArrayList<>();
                if (chatMessages != null && !chatMessages.isEmpty()) {
                    for(ChatMessage chatMessage : chatMessages) {
                        ChatMessageDTO chatMessageDTO = modelMapper.map(chatMessage, ChatMessageDTO.class);
                        chatMessageDTOS.add(chatMessageDTO);
                    }
                }
                return new ResponseDTO(chatMessageDTOS, true, "Chat retrieved successfully", HttpStatus.OK);
            }else{
                return new ResponseDTO(null, false, "No active chat found", HttpStatus.NOT_FOUND);
            }
        }catch (Exception e){
            e.printStackTrace();
            log.error("Error occurred:"+e.getMessage());
            return new ResponseDTO(null, false, "Some error occurred", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseDTO saveUserReply(Long chatId, String message){
        try{
            log.info("Getting chat");
            Chat chat = chatRepository.findById(chatId).orElse(null);
            if(chat == null){
                return new ResponseDTO(null, false, "Chat not found", HttpStatus.NOT_FOUND);
            }
            SimpleDateFormat format = new SimpleDateFormat("HH:mm aa");

            String finalMessage = "Message received\t\t\t\t"+format.format(new Date())+"\n";

            finalMessage += message;

            log.info(finalMessage);

            ChatMessage chatMessage = new ChatMessage();
            chatMessage.setCreatedAt(new Timestamp(new Date().getTime()));
            chatMessage.setChat(chat);
            chatMessage.setStatus(Status.ACTIVE);
            chatMessage.setType(ChatType.RECEIVED);
            chatMessage.setMessage(finalMessage);

            chatMessageRepository.save(chatMessage);

            chat.setStatus(Status.CLOSED);
            chatRepository.save(chat);

            tasksHandler.stopScheduledTask(chatId);

            return new ResponseDTO(null, true, "Response received, chat closed", HttpStatus.OK);

        }catch (Exception e){
            e.printStackTrace();
            log.error("Error occurred:"+e.getMessage());
            return new ResponseDTO(null, false, "Some error occurred", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostConstruct
    public void startJobs(){
        try{
            TransactionTemplate tmpl = new TransactionTemplate(txManager);
            tmpl.execute(new TransactionCallbackWithoutResult() {
                @Override
                protected void doInTransactionWithoutResult(TransactionStatus status) {
                    try {
                        Date startDate = new Date();
                        log.info("Looking for pending jobs");
                        List<Job> jobs = jobRepository.findByStatus(Status.ACTIVE);
                        if (jobs != null && !jobs.isEmpty()) {
                            log.info("Jobs found: " + jobs.size());
                            for (Job job : jobs) {
                                if (job.getCustomer() != null && job.getEmployee() != null) {
                                    Chat chat = chatRepository.findByEmployeeIdAndCustomerIdAndStatus(job.getEmployee().getId(), job.getCustomer().getId(), Status.ACTIVE);
                                    if (chat == null) {
                                        log.info("Starting chat between Customer:"+job.getCustomer().getFirstName()+" and Employee:"+job.getEmployee().getFirstName());
                                        Chat newChat = new Chat();
                                        newChat.setCreatedAt(new Timestamp(new Date().getTime()));
                                        newChat.setCustomer(job.getCustomer());
                                        newChat.setEmployee(job.getEmployee());
                                        newChat.setStatus(Status.ACTIVE);
                                        chat = chatRepository.save(newChat);
                                    }
                                    AsyncSendMessageTask asyncSendMessageTask = taskFactory.getObject();
                                    asyncSendMessageTask.setMessageTask(chat, job.getMessageType(), job.getTemplate());
                                    tasksHandler.scheduleTask(chat.getId(), asyncSendMessageTask, startDate, job.getDelayValue(), job.getTimeUnit().timeUnit);
                                }
                            }
                        }else{
                            log.info("No jobs found");
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                        log.error("Error occurred: "+e.getMessage());
                    }
                }
            });

        }catch (Exception e){
            e.printStackTrace();
            log.error("Error occurred:"+e.getMessage());
        }
    }
}
