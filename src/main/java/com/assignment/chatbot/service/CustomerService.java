package com.assignment.chatbot.service;

import com.assignment.chatbot.dto.CustomerDTO;
import com.assignment.chatbot.dto.ResponseDTO;
import com.assignment.chatbot.model.Customer;
import com.assignment.chatbot.repository.CustomerRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class CustomerService {

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    ModelMapper modelMapper;

    public ResponseDTO getAllCustomers(){
        try{
            List<Customer> customerList = customerRepository.findAll();
            List<CustomerDTO> customerDTOList = new ArrayList<>();
            if(!customerList.isEmpty()){
                for(Customer customer : customerList){
                    CustomerDTO customerDTO = modelMapper.map(customer, CustomerDTO.class);
                    customerDTOList.add(customerDTO);
                }
                return new ResponseDTO(customerDTOList, true, "Get customer successful", HttpStatus.OK);
            }else{
                return new ResponseDTO(null, false, "No customers found", HttpStatus.NOT_FOUND);
            }
        }catch (Exception e){
            e.printStackTrace();
            log.error("Error occurred:"+e.getMessage());
            return new ResponseDTO(null, false, "Some error occurred", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
