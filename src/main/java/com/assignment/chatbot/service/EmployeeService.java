package com.assignment.chatbot.service;

import com.assignment.chatbot.dto.CustomerDTO;
import com.assignment.chatbot.dto.EmployeeDTO;
import com.assignment.chatbot.dto.ResponseDTO;
import com.assignment.chatbot.model.Customer;
import com.assignment.chatbot.model.Employee;
import com.assignment.chatbot.repository.EmployeeRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class EmployeeService {

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    ModelMapper modelMapper;

    public ResponseDTO getAllEmployees(){
        try{
            List<Employee> employeeList = employeeRepository.findAll();
            List<EmployeeDTO> employeeDTOList = new ArrayList<>();
            if(!employeeList.isEmpty()){
                for(Employee employee : employeeList){
                    EmployeeDTO employeeDTO = modelMapper.map(employee, EmployeeDTO.class);
                    employeeDTOList.add(employeeDTO);
                }
                return new ResponseDTO(employeeDTOList, true, "Get employee successful", HttpStatus.OK);
            }else{
                return new ResponseDTO(null, false, "No employees found", HttpStatus.NOT_FOUND);
            }
        }catch (Exception e){
            e.printStackTrace();
            log.error("Error occurred:"+e.getMessage());
            return new ResponseDTO(null, false, "Some error occurred", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
