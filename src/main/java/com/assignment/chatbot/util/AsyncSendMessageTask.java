package com.assignment.chatbot.util;

import com.assignment.chatbot.enums.*;
import com.assignment.chatbot.model.Chat;
import com.assignment.chatbot.model.ChatMessage;
import com.assignment.chatbot.model.Job;
import com.assignment.chatbot.repository.ChatMessageRepository;
import com.assignment.chatbot.repository.JobRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j
@Component
@Scope("prototype")
public class AsyncSendMessageTask implements Runnable{

    @Autowired
    private ChatMessageRepository chatMessageRepository;

    @Autowired
    private ScheduledTasksHandler scheduledTasksHandler;

    @Autowired
    private JobRepository jobRepository;

    private Chat chat;
    private MessageType messageType;
    private MessageTemplate template;

    private SimpleDateFormat formatter = new SimpleDateFormat("HH:mm aa");
    @Override
    public void run() {
        try {
            log.info("----------------Scheduled task started");
            Date currentDate = new Date();
            String message = "Message Sent via:"+messageType.value+"\t\t\t\t"+formatter.format(currentDate)+"\n";
            if (template != null) {
                message += template.message.replace("$customerFirstName", chat.getCustomer().getFirstName())
                        .replace("$employeeFirstName", chat.getEmployee().getFirstName());
            }

            ChatMessage chatMessage = new ChatMessage();
            chatMessage.setCreatedAt(new Timestamp(currentDate.getTime()));
            chatMessage.setChat(chat);
            chatMessage.setStatus(Status.ACTIVE);
            chatMessage.setType(ChatType.SEND);
            chatMessage.setMessage(message);

            chatMessageRepository.save(chatMessage);

            log.info(message);

            Job job = jobRepository.findByStatusAndCustomerIdAndEmployeeIdAndTemplate (Status.ACTIVE, chat.getCustomer().getId(), chat.getEmployee().getId(), template);
            if(job != null){
                log.info("Job closed");
                job.setStatus(Status.CLOSED);
                jobRepository.save(job);
            }

//            scheduledTasksHandler.stopScheduledTask(chat.getId(), this.template, this);
        }catch (Exception e){
            e.printStackTrace();
            log.error("Error occurred: "+e.getMessage());
        }
    }

    public void setMessageTask(Chat chat, MessageType messageType, MessageTemplate template){
        this.chat = chat;
        this.messageType = messageType;
        this.template = template;
    }
}
