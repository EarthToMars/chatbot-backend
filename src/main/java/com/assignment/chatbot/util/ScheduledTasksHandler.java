package com.assignment.chatbot.util;

import com.assignment.chatbot.enums.MessageTemplate;
import com.assignment.chatbot.enums.Status;
import com.assignment.chatbot.model.Chat;
import com.assignment.chatbot.model.Job;
import com.assignment.chatbot.repository.ChatRepository;
import com.assignment.chatbot.repository.JobRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.support.ScheduledMethodRunnable;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.*;

@Component
public class ScheduledTasksHandler {

    @Autowired
    private TaskScheduler taskScheduler;

    @Autowired
    private ChatRepository chatRepository;

    @Autowired
    private JobRepository jobRepository;

    private Map<Long, List<ScheduledFuture<?>>> scheduledTasks = new HashMap<>();

    public void scheduleTask(Long chatId, Runnable task, Date startDate, int delay, int timeUnit) throws Exception{
        Calendar c = Calendar.getInstance();
        c.setTime(startDate);
        c.add(timeUnit, delay);
        Date schedulesDate= c.getTime();
        ScheduledFuture<?> future = taskScheduler.schedule(task,schedulesDate);
        List<ScheduledFuture<?>> futureList = scheduledTasks.getOrDefault(chatId, new ArrayList<>());
        futureList.add(future);
        scheduledTasks.put(chatId, futureList);
    }

//    public void stopScheduledTask(Long chatId, MessageTemplate template, AsyncSendMessageTask asyncSendMessageTask) throws Exception{
//        List<ScheduledFuture<?>> futureList = scheduledTasks.get(chatId);
//        if(futureList != null && !futureList.isEmpty()) {
//            ScheduledFuture<?> requiredFuture = null;
//            for(ScheduledFuture<?> future : futureList){
//                if(future.get().equals(asyncSendMessageTask)){
//                    requiredFuture = future;
//                    break;
//                }
//            }
//            requiredFuture.cancel(true);
//            futureList.remove(requiredFuture);
//            if(futureList.isEmpty()){
//                scheduledTasks.remove(chatId);
//                Chat chat = chatRepository.findById(chatId).orElse(null);
//                if (chat != null) {
//                    chat.setStatus(Status.CLOSED);
//                    chatRepository.save(chat);
//
//                    Job job = jobRepository.findByStatusAndCustomerIdAndEmployeeIdAndTemplate (Status.ACTIVE, chat.getCustomer().getId(), chat.getEmployee().getId(), template);
//                    if(job != null){
//                        job.setStatus(Status.CLOSED);
//                        jobRepository.save(job);
//                    }
//                }
//            }
//        }
//    }

    public void stopScheduledTask(Long chatId) throws Exception{
        List<ScheduledFuture<?>> futureList = scheduledTasks.get(chatId);
        if(futureList != null && !futureList.isEmpty()) {
            for (ScheduledFuture<?> future : futureList) {
                future.cancel(true);
            }
        }
        Chat chat = chatRepository.findById(chatId).orElse(null);
        if (chat != null) {
            chat.setStatus(Status.CLOSED);
            chatRepository.save(chat);

            List<Job> jobs = jobRepository.findByStatusAndCustomerIdAndEmployeeId(Status.ACTIVE, chat.getCustomer().getId(), chat.getEmployee().getId());

            if(jobs != null && !jobs.isEmpty()){
                for(Job job : jobs){
                    job.setStatus(Status.CLOSED);
                    jobRepository.save(job);
                }
            }
        }
        scheduledTasks.remove(chatId);
    }
}
